<?php
/**
 * Boutique engine room
 *
 * @package boutique
 */

/**
 * Set the theme version number as a global variable
 */
$theme				= wp_get_theme( 'boutique' );
$boutique_version	= $theme['Version'];

$theme				= wp_get_theme( 'storefront' );
$storefront_version	= $theme['Version'];

/**
 * Load the individual classes required by this theme
 */
require_once( 'inc/class-boutique.php' );
require_once( 'inc/class-boutique-customizer.php' );
require_once( 'inc/class-boutique-template.php' );
require_once( 'inc/class-boutique-integrations.php' );

/**
 * Mục lục
 * 1 - Liên quan đến Layout: Tạo menu cho theme
 * 2 - Liên quan đến sidebar, widget: Tạo sidebar cho theme, 
 * 3 - Liên quan đến view content: Tự bôi đen paragraph đầu của bài viết
 * 4 - Liên quan đến plugin: jetpack
 * 5 - Liên quan đến shop woo
 */

/*
* Tạo menu cho theme
*/
register_nav_menu ( 'top-left-menu', __('Top left Menu', 'lacvietmedia') );
register_nav_menu ( 'top-right-menu', __('Top right Menu', 'lacvietmedia') );
register_nav_menu ( 'bottom-menu', __('Bottom Menu', 'lacvietmedia') );
/* -----------------------

/*
* Tạo sidebar cho theme
*/
$sidebar = array(
   'name' => __('Bottom', 'Bottom'),
   'id' => 'Copy-right-sidebar-bottom',
   'description' => 'Bottom',
   'class' => 'main-sidebar',
   'before_title' => '<h3 class="widgettitle">',
   'after_title' => '</h3>'
);
register_sidebar( $sidebar ); 



/* -----------------------

/**
 * Change jetpact plugin
 * Change the “Related” headline at the top of the Related Posts section
 */
function jetpackme_related_posts_headline( $headline ) {
$headline = sprintf(
            '<h3 class="jp-relatedposts-headline"><em>%s</em></h3>',
            esc_html( 'Bài viết liên quan' )
            );
return $headline;
}
add_filter( 'jetpack_relatedposts_filter_headline', 'jetpackme_related_posts_headline' );

/**
 * Change jetpact plugin 
 * Change the number of Related Posts displayed under your posts 
 */
function jetpackme_more_related_posts( $options ) {
    $options['size'] = 3;
    return $options;
}
add_filter( 'jetpack_relatedposts_filter_options', 'jetpackme_more_related_posts' );

function jetpackme_no_related_posts( $options ) {
    if ( is_single( array( 17, 19, 1, 11 ) ) ) {
        $options['enabled'] = false;
    }
    return $options;
}
add_filter( 'jetpack_relatedposts_filter_options', 'jetpackme_no_related_posts' );

//Remove Related Posts from WooCommerce Products
function exclude_jetpack_related_from_products( $options ) {
    if ( is_product() ) {
        $options['enabled'] = false;
    }
 
    return $options;
}
 
add_filter( 'jetpack_relatedposts_filter_options', 'exclude_jetpack_related_from_products' );
//----------

//Remove copyright
//---------- 
add_action( 'init', 'custom_remove_footer_credit', 10 );

function custom_remove_footer_credit () {
    remove_action( 'storefront_footer', 'storefront_credit', 20 );
}
 
//5 - Liên quan đến shop woo

// remove default sorting dropdown in StoreFront Theme
add_action('init','delay_remove');
function delay_remove() {
remove_action( 'woocommerce_after_shop_loop', 'woocommerce_catalog_ordering', 10 );
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 10 );
}
//Code to Remove “Showing All … Results” Text from WooCommerce Product, Catalog, and Shop Pages
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );

//---------- 

/**
 * Do not add custom code / snippets here.
 * While Child Themes are generally recommended for customisations, in this case it is not
 * wise. Modifying this file means that your changes will be lost when an automatic update
 * of this theme is performed. Instead,  add your customisations to a plugin such as
 * https://github.com/woothemes/theme-customisations
 */